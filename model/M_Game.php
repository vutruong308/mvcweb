<?php
    require_once("E_Game.php");
    require_once(__DIR__."/../connection.php");
    class M_Game
    {

        public function getAllGames()
        {
            $gamelist = array();
            $sql = "SELECT * FROM game";

            $req = Database::getDb()->prepare($sql);
            $req->execute();
            return $req->fetchAll() ;
        }

        public function getGames($title)
        {
            $allGame = $this->getAllGames($title);
            return $allGame[$title];
        }

        public function getGame($title)
        {
            $allGame = $this->getAllGames();
            foreach ($allGame as $game)
            {
                if ($game['title'] == $title)
                    return $game;
            }
        }
    }
