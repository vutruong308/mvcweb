<?php
class Game
{
    protected  $id;
    protected  $title;
    protected $producer;
    protected $price;
    protected  $image;

    public function __construct($_id, $_title, $_producer,$_price, $_image)
    {
        $this->id = $_id;
        $this->title = $_title;
        $this->producer = $_producer;
        $this->price = $_price;
        $this->image = $_image;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getProducer()
    {
        return $this->producer;
    }

}

