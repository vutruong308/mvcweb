<?php
    require_once (__DIR__. '/../model/M_Game.php');
    class GamesController
    {
        public $db;

        /**--
         * GamesController constructor.
         * @param $db
         */
        public function __construct()
        {
            $this->db = new M_Game();
//            $this->folder = "games";
//            $this->render("games");
        }

        //Get All Gamesames
        public function viewAll()
        {
            $games = $this->db->getAllGames();
            include 'view/gameslist.php';

        }

        //Get One specifice game
        public function view()
        {
            $game = $this->db->getGame($_GET['title']);
            include 'view/viewgame.php';
        }
    }
?>
