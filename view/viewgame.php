

<html lang="en">
<head>
    <title>View game</title>
    <style>
        <?php  include __DIR__.'/../assets/stylesheets/main.css'; ?>
    </style>
</head>
<body>

    <?php
//        echo 'Title:'. $game['title'];
        echo '<span class="row image-txt-container bg-light border no-gutters">
                <div class="col-md-5 image-item child">
                    <img class="w-100" src="assets/images/'.$game['image'].'" src="'.$game['image'].'"/>
                </div>    
                <div class="col-md-3 child">
                    <h5 class="mt-0">'.$game['title'] .'</h5>
                    <div><strong>Producer:</strong><span class="card-text">'. $game['producer']. '</span></div>
                    <div><strong>Price:</strong><span class="card-text">'. $game['price']. '</span></div>
                </div>   
             </span>'
    ?>
</body>
</html>