<!DOCTYPE html>
<html lang="en">
<head>
    <title>GameList</title>
    <style>
        <?php  include __DIR__.'/../assets/stylesheets/main.css'; ?>
    </style>
</head>
<body>
        <?php
        echo '
                <div class="row">
                    <div class ="card-deck">';
        if (is_array($games) || is_object($games))
        {
            foreach ($games as $key => $game)
            {
                echo '<div class="card border" >
                       <a href="index.php?title='.$game['title'].'" class="custom-card"/>        
                       <img class="card-img-top" src="assets/images/'.$game['image'].'" alt="'.$game['image'].'"/>
                            <div class="card-body">
                                <div><strong>Title:</strong><span class="card-text">'. $game['title']. '</span></div>
                                <div><strong>Producer:</strong><span class="card-text">'. $game['producer']. '</span></div>
                                <div><strong>Price:</strong><span class="card-text">'. $game['price']. '</span></div>
                            </div>  
                      </div>';
            }
        }
        echo    '</div>
                </div>';
        ?>
<!--</table>-->
</body>
</html>